package com.stephenkurlow.calculator.steps;

import com.stephenkurlow.calculator.model.CalculatorAddQuestion;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

/**
 * Class com.stephenkurlow.calculator.steps.CalculatorSteps.
 */
public class CalculatorSteps {

    private ResponseEntity<String> stringResponseEntity;

    @Given("^Calculator is ready$")
    public void Calculator_is_ready() throws Throwable {
        // There is nothing at this stage that is needed to get the calculator ready.
    }

    @When("^I ask the Calculator (\\d+) plus (\\d+)$")
    public void I_ask_the_Calculator_plus(int leftOperand, int rightOperand) throws Throwable {
        final RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new MappingJackson2HttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);
        final CalculatorAddQuestion calculatorAddQuestion = new CalculatorAddQuestion();
        calculatorAddQuestion.setNumber1(leftOperand);
        calculatorAddQuestion.setNumber2(rightOperand);
        stringResponseEntity = restTemplate.postForEntity("http://localhost:8080/app/calculator/addTwoNumbers", calculatorAddQuestion, String.class);
    }

    @Then("^the Calculator tells me the answer is (\\d+)$")
    public void the_Calculator_tells_me_the_answer_is(int expectedAnswer) throws Throwable {
        assertThat(stringResponseEntity.getBody(), is(equalTo(String.valueOf(expectedAnswer))));
    }
}
