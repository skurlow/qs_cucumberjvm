Feature: Calculate the addition of two numbers

Scenario: Add two whole numbers

Given Calculator is ready
When I ask the Calculator 5 plus 3
Then the Calculator tells me the answer is 8