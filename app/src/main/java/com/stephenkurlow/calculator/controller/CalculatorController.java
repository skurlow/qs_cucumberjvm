package com.stephenkurlow.calculator.controller;

import com.stephenkurlow.calculator.model.CalculatorAddQuestion;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Class CalculatorController.
 */
@Controller
@RequestMapping("/calculator")
public class CalculatorController {

    @RequestMapping(value = "/addTwoNumbers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> addTwoNumbers(@RequestBody CalculatorAddQuestion calculatorAddQuestion) {
        final int answer = calculatorAddQuestion.getNumber1() + calculatorAddQuestion.getNumber2();
        return new ResponseEntity<String>(String.valueOf(answer), HttpStatus.OK);
    }
}
